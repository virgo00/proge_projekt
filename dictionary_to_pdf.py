def table(dict,file_name,type):
    
    file_name = file_name + ".pdf"
    
    import matplotlib.pyplot as plt
    import numpy as np
    from matplotlib.backends.backend_pdf import PdfPages
    import datetime
    if type == "pie":
        values = list(dict.values())
        values.index(max(values))
        explode = []
        for i in range(len(values)):
            if i == values.index(min(values)):
                explode.append(0.2)
            else:
                explode.append(0)
        explode = tuple(explode)

        
       
        with PdfPages(file_name) as pdf:
            plt.pie(dict.values(),explode = explode, labels=dict.keys(), autopct='%1.1f%%',shadow=True, startangle=90)
            #plt.xlabel('kasutaja')
            #plt.ylabel('kasutustunnid')
            #plt.xticks(range(len(dict)), list(dict.keys()))
            plt.title('Kasutajad')
            pdf.savefig()
    else:
        plt.bar(range(len(dict)), dict.values(), align='center')
        plt.xlabel('kasutaja')
        plt.ylabel('kasutustunnid')
        plt.xticks(range(len(dict)), list(dict.keys()))
        with PdfPages(file_name) as pdf:
            plt.bar(range(len(dict)), dict.values(), align='center')
            plt.xlabel('kasutaja')
            plt.ylabel('kasutustunnid')
            plt.xticks(range(len(dict)), list(dict.keys()))
            plt.title('Kasutajad')
            pdf.savefig()
        
dict= {"esimene":10,"teine":25,"kolmas":17,"neljas":14,"viies":67}
table(dict,"kasutajagrupp1","pie")